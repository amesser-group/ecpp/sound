/*
 *  Copyright 2022 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project ECPP is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_SOUND_MIDI_RECEIVER_HPP_
#define ECPP_SOUND_MIDI_RECEIVER_HPP_

#include <cstdint>
#include "ecpp/Ringbuffer.hpp"

namespace ecpp { namespace Sound { namespace Midi
{
  /** Stores atmost one midi message in a simple array */
  class SingleBuffer
  {
  public:
    class Writer
    {
    public:
      constexpr Writer(SingleBuffer &buffer) : msg_(buffer.msg_) {};

      void set_status(uint8_t status)               { msg_[0] = status & 0x7F;}
      void set_data  (uint8_t index, uint8_t data)  { msg_[index + 1] = data; }

      void Commit(uint8_t length)                   { msg_[0] |= 0x80; }

      constexpr uint8_t space()                     { return (msg_[0] & 0x80) ? 0 : sizeof(msg_); } 

    protected:
      uint8_t (&msg_)[3];
    };

    class Reader
    {
    public:
      constexpr Reader(SingleBuffer &buffer) : msg_(buffer.msg_) {};

      uint8_t status() const            { return msg_[0]; }
      int8_t  data(uint8_t index) const { return static_cast<int8_t>(msg_[index + 1]); }

      void next()                       { msg_[0] = 0; }
    protected:
      uint8_t (&msg_)[3];
    };


    Writer writer() { return Writer(*this); }
    Reader reader() { return Reader(*this); }

  protected:
    uint8_t msg_[3];
  };


  /** Stores received midi messages in a ring buffer */
  template<unsigned long CNT>
  class RingBuffer : protected ::ecpp::Ringbuffer<uint8_t, CNT>
  {
  public:
    using Ring = ::ecpp::Ringbuffer<uint8_t, CNT>;

    using Ring::write;
    using Ring::commit;
    using Ring::pop;
    using Ring::size;
    using Ring::space;
  };

  template<typename B>
  class Receiver
  {
  public:
    /** Buffer used to store received messages */
    using Buffer = B;

    Buffer & buffer() { return rx_buffer_; }

    uint8_t ProcessCharacter(uint8_t rx_data)
    {
      uint8_t retval = 0;

      if (rx_data & 0x80)
      { /* this is a status byte */
        if ((rx_data & 0xF8) == 0xF8)
        { /* real-time messages can interrupt anything
          * skip them for now */
    
        }
        else
        {
          running_status_ = rx_data;
          third_byte_ = false;
        }
      }
      else if (running_status_)
      {
        auto writer = rx_buffer_.writer();

        if (third_byte_)
        {
          third_byte_ = false;

          writer.set_data(1, rx_data);
          writer.Commit(3);

          retval = running_status_;
        }
        else
        {
          if(running_status_ < 0xC0)
            third_byte_ = true;
          else if((running_status_ >= 0xE0) && (running_status_ < 0xF0))
            third_byte_ = true;
          else if(running_status_ == 0xF2)
            third_byte_ = true;
      
          if((running_status_ > 0xF3) ||
            (running_status_ == 0xF0))
          {
            running_status_ = 0;
          }
          else if(third_byte_)
          {
            if(writer.space() >= 3)
            {
              writer.set_status(running_status_);
              writer.set_data(0, rx_data);
            }
            else
            { /* ring overrun, wait for next status */
              running_status_ = 0;
            }
          }
          else
          {
            if(writer.space() >= 2)
            {
              writer.set_status(running_status_);
              writer.set_data(0, rx_data);
              writer.Commit(2);

              retval = running_status_;
            }
            else
            { /* ring overrun, wait for next status */
              running_status_ = 0;
            }
          }
      
          if(running_status_ >= 0xF0)
            running_status_ = 0;
        }
      }

      return retval;
    }
  private:
    Buffer          rx_buffer_;
    uint_least8_t   running_status_ = 0;
    uint_least8_t   third_byte_     = false;
  };

}; }; };

#endif

