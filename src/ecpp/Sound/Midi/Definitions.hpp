/*
 *  Copyright 2022 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project ECPP is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_SOUND_MIDI_DEFINITIONS_HPP_
#define ECPP_SOUND_MIDI_DEFINITIONS_HPP_

#include <cstdint>

namespace ecpp { namespace Sound { namespace Midi
{
  /* Serial Baud Rate for Midi 1 */
  static constexpr uint16_t kBaudRate = 31250;

  static constexpr uint8_t kStatusNoteOff       = 0x80;
  static constexpr uint8_t kStatusNoteOn        = 0x90;
  static constexpr uint8_t kStatusControlChange = 0xB0;
  static constexpr uint8_t kStatusPitchBend     = 0xE0;

  static constexpr uint8_t kPitchBendCenter     = 0x40;

  /* expression Control */
  static constexpr uint8_t kControllerExpression = 11;


  namespace Note {
    static constexpr int8_t kA0   = 21;
    static constexpr int8_t kC1   = 24;
    static constexpr int8_t kGb1  = 30;
    static constexpr int8_t kB7   = 107;
  }

}; }; };

#endif

