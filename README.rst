##################
ECPP sound library
##################

ECPP sound processing & synthesis related function
library


License
=======

All source code files are published under the terms of
GNU General Public License version 3 with linking 
exception.

Copyright
=========

For any source code files if not otherwise stated in file header
Copyright is (c) 2022 Andreas Messer <andi@bastelmap.de>

References
==========

.. target-notes::
